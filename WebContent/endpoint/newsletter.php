<?php 
require_once '../dao.php';

header('Content-Type: application/json');	// send json back

function parseJsonBody(){
	$bodyContent = file_get_contents('php://input'); // POST body raw content
	$entityBody = json_decode($bodyContent == "" ? "{}" : $bodyContent);
	return $entityBody;
}

function handlePathInfo($action){
	if(isset($_SERVER['PATH_INFO'])){
		$objectId = substr($_SERVER['PATH_INFO'], 1);
		echo $action($objectId);
	} else {
		http_response_code(400);
	}

}

// REST PARADIGM 
$apiMethod = $_SERVER['REQUEST_METHOD'];
switch($apiMethod){
	case 'GET':
		handlePathInfo(function($id){
			$dao = new EmailDao('localhost', 'root', '');
			$databaseResult = $dao->execute("READ", array(':id' => $id));
			if(empty($databaseResult)){
				http_response_code(400);
				return '{"error" : "No such object!"}';
			} else {
				return json_encode($databaseResult[0]);
			}
		});
		break;
	case 'POST':
		$dao = new EmailDao('localhost', 'root', '');
		$bodyDecoded = parseJsonBody();
		$idArray = $dao->execute("CREATE", array(':email' => $bodyDecoded->email));
		echo json_encode($idArray);
		break;
	case 'PUT':
		handlePathInfo(function($id){
			$dao = new EmailDao('localhost', 'root', '');
			$bodyDecoded = parseJsonBody();
			return json_encode($dao->execute("UPDATE", array(':id' => $id, ':email' => $bodyDecoded->email)));
		});
		break;
	case 'DELETE':
		handlePathInfo(function($id){
			$dao = new EmailDao('localhost', 'root', '');
			return json_encode($dao->execute("DELETE", array(':id' => $id)));
		});
		break;
}
?>