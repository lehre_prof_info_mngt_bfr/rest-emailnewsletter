<?php 
	class EmailDao {
		private static $insertStmtString = "INSERT INTO emails(email) VALUES (:email)";
		private static $selectStmtString = "SELECT * FROM emails WHERE id=:id";
		private static $selectAllStmtString = "SELECT * FROM emails";
		private static $updateStmtString = "UPDATE emails SET email=:email WHERE id=:id";
		private static $deleteStmtString = "DELETE FROM emails WHERE id=:id";
		
		private $con, $stmts; 
		
		public function __construct($host, $user, $pw){
			$this->con = new PDO('mysql:host=' . $host . ';dbname=webtech2', $user, $pw);
			$this->stmts = array(
				'CREATE' => $this->con->prepare(EmailDao::$insertStmtString),
				'READ' => $this->con->prepare(EmailDao::$selectStmtString),
				'READALL' => $this->con->prepare(EmailDao::$selectAllStmtString),
				'UPDATE' => $this->con->prepare(EmailDao::$updateStmtString),
				'DELETE' => $this->con->prepare(EmailDao::$deleteStmtString)
			);
		}

		function execute($method, $params = array()){
			$stmt = $this->stmts[$method];
			if(!empty($params)){
				foreach($params as $key => $value){
			        $stmt->bindValue($key, $value);
			    }
			}
			$stmt->execute();
			switch($method){
				case "READ": return $stmt->fetchAll(PDO::FETCH_ASSOC);
				break;
				case "CREATE" : return array('id' => $this->con->lastInsertId());
				break;
				default : return array('id' => $params[':id']);
			}
		}
	}
	
	function test_dao(){
		$emailDao = new EmailDao('localhost', 'root', '');
		$idArray = $emailDao->execute("CREATE", array(':email'=> 'adsfadsfdsfs@wifa.uni-leipzig.de'));
		$id = $idArray['id'];
		echo '<p>';
		print_r($id);
		echo '<p>';
		print_r($emailDao->execute("READ", array(':id' => $id)));
		echo '<p>';
		print_r($emailDao->execute("UPDATE", array(':id' => $id, ':email'=> 'asdf@wifa.uni-leipzig.de')));
		echo '<p>';
		print_r($emailDao->execute("DELETE", array(':id' => $id)));
	}
	
?>